﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Concurrent;
using System.Security;
using System.Linq;

namespace prebetaasm
{
    namespace Win32MemoryManagementTools
    {


        /// <summary>
        /// Управляемая обёртка над дескриптором кучи из библиотеки kernel32.dll, работоспособна только под Win32
        /// </summary>
        [SecurityCritical]
        public sealed class SafeWin32HeapHandle : SafeHandle, IHeap
        {

            public SafeWin32HeapHandle(Win32HeapOptions options)
                : base(IntPtr.Zero, true)
            { handle = HeapCreate(options, UIntPtr.Zero, UIntPtr.Zero); }

            public SafeWin32HeapHandle(Win32HeapOptions options, UIntPtr initSize, UIntPtr maxSize)
                : base(IntPtr.Zero, true)
            { handle = HeapCreate(options, initSize, maxSize); }

            /// <summary>
            /// Создаёт кучу неявно, после чего возвращает умный указатель на неё.
            /// </summary>
            /// <param name="opts">Параметры для создания кучи</param>
            /// <param name="size">Размер кучи</param>
            /// <returns>
            /// Умный указатель на неуправляемый блок памяти. Может быть изменён в пределах размеров своей кучи,
            /// удалён вызовом Dispose или Destroy(true) на себе.
            /// </returns>
            public unsafe static SmartPtr GetImplicitHeapMemory(Win32HeapOptions opts, int size)
            {
                SafeWin32HeapHandle heap = new SafeWin32HeapHandle(opts);
                return new SmartPtr(HeapAlloc(heap.handle, default(Win32HeapOptions), (UIntPtr)size), heap.FreeMethodForImplicit);
            }

            private void FreeMethod(SmartPtr x)
            { HeapFree(handle, default(Win32HeapOptions), x); }

            private void FreeMethodForImplicit(SmartPtr s)
            { Dispose(HeapFree(handle, default(Win32HeapOptions), s)); }

            public unsafe SmartPtr AllocHeap(int size)
            { return new SmartPtr(HeapAlloc(handle, default(Win32HeapOptions), (UIntPtr)size), FreeMethod); }

            /// <summary>
            /// Освобождает память, выделенную посредством AllocHeap
            /// </summary>
            /// <param name="block">
            /// Умный указатель на память, которую необходимо освободить.
            /// Её также можно освободить вызовом Dispose или Destroy(true) данного экземпляра указателя.</param>
            public unsafe void FreeHeap(SmartPtr block)
            {
                block.Finalization -= FreeMethod;
                FreeMethod(block);
            }

            public override bool IsInvalid { get { return handle == IntPtr.Zero; } }

            protected override bool ReleaseHandle()
            { return HeapDestroy(handle); }


            /// <summary>
            /// Изменяет размер блока памяти, выделенного посредством AllocHeap
            /// </summary>
            /// <param name="oldmemory">Указатель на старую память</param>
            /// <param name="newSize">Желаемый рамер обновлённой памяти</param>
            /// <returns>Указатель на обновлённую память</returns>
            public unsafe SmartPtr ReAllocHeap(SmartPtr oldmemory, int newSize)
            {
                oldmemory.Finalization -= FreeMethod;
                return new SmartPtr(HeapReAlloc(handle, default(Win32HeapOptions), oldmemory, (UIntPtr)newSize), FreeMethod);
            }


            [DllImport("kernel32.dll", SetLastError = true)]
            private static extern IntPtr HeapCreate(Win32HeapOptions opt, UIntPtr initSize, UIntPtr maxSize);

            [DllImport("kernel32.dll", SetLastError = true)]
            private static extern bool HeapDestroy(IntPtr h);

            [DllImport("kernel32.dll", SetLastError = false)]
            private static extern UIntPtr HeapAlloc(IntPtr h, Win32HeapOptions flags, UIntPtr size);


            [DllImport("kernel32.dll", SetLastError = false)]
            private static extern bool HeapFree(IntPtr h, Win32HeapOptions opts, SmartPtr block);

            [DllImport("kernel32.dll", SetLastError = false)]
            private static extern UIntPtr HeapReAlloc(IntPtr heap, Win32HeapOptions flags, SmartPtr oldmemory, UIntPtr size);
        }



        [Flags]
        public enum Win32HeapOptions : uint
        { EnableExceptions = 4, EnableExecute = 0x40000, ZeroMemory = 8, NoSerialize = 1 }
    }
}
