﻿using System;

namespace prebetaasm
{
    /// <summary>
    /// Предоставляет опкоды регистров процессора x86
    /// </summary>
    [ISAProvider]
    public class IA
    {
        private IA()
        { throw new System.InvalidCastException(); }

        public static readonly CPURegisterData<byte, IA> AL = new B_REG(0);
        public static readonly CPURegisterData<byte, IA> CL = new B_REG(1);
        public static readonly CPURegisterData<byte, IA> DL = new B_REG(2);
        public static readonly CPURegisterData<byte, IA> BL = new B_REG(3);
        public static readonly CPURegisterData<byte, IA> AH = new B_REG(4);
        public static readonly CPURegisterData<byte, IA> CH = new B_REG(5);
        public static readonly CPURegisterData<byte, IA> DH = new B_REG(6);
        public static readonly CPURegisterData<byte, IA> BH = new B_REG(7);
        public static readonly CPURegisterData<ushort, IA> AX = new W_REG(0);
        public static readonly CPURegisterData<ushort, IA> CX = new W_REG(1);
        public static readonly CPURegisterData<ushort, IA> DX = new W_REG(2);
        public static readonly CPURegisterData<ushort, IA> BX = new W_REG(3);

        [Obsolete(StackError, true)]
        public static readonly CPURegisterData<ushort, IA> SP = new W_REG(4);

        public static readonly CPURegisterData<ushort, IA> BP = new W_REG(5);
        public static readonly CPURegisterData<ushort, IA> SI = new W_REG(6);
        public static readonly CPURegisterData<ushort, IA> DI = new W_REG(7);
        public static readonly CPURegisterData<uint, IA> EAX = new DW_REG(0);
        public static readonly CPURegisterData<uint, IA> ECX = new DW_REG(1);
        public static readonly CPURegisterData<uint, IA> EDX = new DW_REG(2);
        public static readonly CPURegisterData<uint, IA> EBX = new DW_REG(3);
        public static readonly CPURegisterData<uint, IA> EBP = new DW_REG(5);
        public static readonly CPURegisterData<uint, IA> ESI = new DW_REG(6);
        public static readonly CPURegisterData<uint, IA> EDI = new DW_REG(7);

        [Obsolete(StackError, true)]
        public static readonly CPURegisterData<uint, IA> ESP = new DW_REG(4);


        public static readonly CPURegisterData<ulong, IA> RAX = new LONG_REG(0);
        public static readonly CPURegisterData<ulong, IA> RCX = new LONG_REG(1);
        public static readonly CPURegisterData<ulong, IA> RDX = new LONG_REG(2);
        public static readonly CPURegisterData<ulong, IA> RBX = new LONG_REG(3);
        public static readonly CPURegisterData<ulong, IA> RBP = new LONG_REG(5);
        public static readonly CPURegisterData<ulong, IA> RSI = new LONG_REG(6);
        public static readonly CPURegisterData<ulong, IA> RDI = new LONG_REG(7);

        [Obsolete(StackError, true)]
        public static readonly CPURegisterData<ulong, IA> RSP = new LONG_REG(4);



        public const CPUFlagData ZF = (CPUFlagData)0x14, SF = (CPUFlagData)0x18, CF = (CPUFlagData)0x12, OF = (CPUFlagData)0x10;
        private const string StackError = "На данный момент SIB и низкоуровневые операции со стеком не поддерживаются";

        private class B_REG : CPURegisterData<byte, IA>
        {
            public B_REG(byte opcode)
                : base(opcode)
            { return; }
        }





        private class W_REG : CPURegisterData<ushort, IA>
        {
            public W_REG(byte opcode)
                : base(opcode)
            { return; }
        }

        /// <summary>
        /// Предоставляет байт MRM для использования с указателями
        /// </summary>
        /// <param name="reg">Операнд 1</param>
        /// <returns>Байт MRM с кодом операнда 1 в поле reg, и кодом регистра, отвечающего за прямую адресацию, в поле rm</returns>
        public static byte MRM(CPURegisterData<IA> reg)
        { return unchecked((byte)((reg.Opcode << 3) | BP.Opcode)); }







        /// <summary>
        /// Предоставляет байт MRM для косвенной адресации со смещением
        /// </summary>
        /// <param name="compactoffset">Если true, то смещение должно задаваться одним байтом, если false, то смещение задаётся машинным словом</param>
        /// <param name="reg">
        /// Первый операнд
        /// </param>
        /// <param name="rm">Регистр, выполняющий роль второго операнда</param>
        /// <returns>Байт MRM для косвенной адресации со смещением</returns>
        public static unsafe byte MRM(bool compactoffset, CPURegisterData<IA> reg, CPURegisterData<IA> rm)
        { return unchecked((byte)((0x80 >> *(byte*)&compactoffset) | (reg.Opcode << 3) | rm.Opcode)); }


        /// <summary>
        /// Предоставляет байт MRM для операций с двумя регистрами
        /// </summary>
        /// <param name="reg">
        /// Первый операнд
        /// </param>
        /// <param name="rm">Второй операнд</param>
        /// <returns>Байт MRM</returns>
        public static byte MRM(CPURegisterData<IA> reg, CPURegisterData<IA> rm)
        { return unchecked((byte)(0xc0 | (reg.Opcode << 3) | rm.Opcode)); }






        private class DW_REG : CPURegisterData<uint, IA>
        {
            public DW_REG(byte opcode)
                : base(opcode)
            { return; }
        }



        private class LONG_REG : CPURegisterData<ulong, IA>
        {
            public LONG_REG(byte opcode)
                : base(opcode)
            { return; }
        }
    }
}
