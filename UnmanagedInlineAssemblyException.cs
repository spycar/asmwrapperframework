﻿
namespace prebetaasm
{
    public class UnmanagedInlineAssemblyException : System.Exception
    {
        public UnmanagedInlineAssemblyException()
            : base()
        { return; }

        public UnmanagedInlineAssemblyException(string message)
            : base(message)
        { return; }
    }
}
