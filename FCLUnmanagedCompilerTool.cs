﻿using System.Collections.Generic;

namespace prebetaasm
{

    /// <summary>
    /// Реализация NativeCompilerTool с использованием теоретически платформонезависимой Библиотеки классов .NET Framework версии 4.0 и выше
    /// </summary>
    /// <typeparam name="Tfunc">Результат компиляции. TFunc должен быть делегатом.</typeparam>
    [System.Security.SecurityCritical]
    public class FCLUnmanagedCompilerTool<Tfunc> : NativeCompilerTool<Tfunc>
        where Tfunc : class
    {
        /// <summary>
        /// Создаёт NativeCompilerTool, используя в качестве временного буфера стек
        /// </summary>
        public FCLUnmanagedCompilerTool(Stack<byte> s)
            : base(s)
        { return; }

        /// <summary>
        /// Создаёт NativeCompilerTool, используя в качестве временного буфера массив фиксированной длины
        /// </summary>
        /// <param name="bytes">Массив</param>
        /// <param name="i">Индекс, начиная с которого производится работа с массивом.</param>
        public FCLUnmanagedCompilerTool(byte[] bytes, int i = 0)
            : base(bytes, i)
        { return; }

        /// <summary>
        /// Создаёт NativeCompilerTool без использования временного буфера, при условии что предполагаемый размер кода в байтах известен и задан заранее
        /// </summary>
        public FCLUnmanagedCompilerTool(int i)
            : base(i)
        { return; }

        /// <summary>
        /// Создаёт NativeCompilerTool, используя в качестве временного буфера реализацию интерфейса IList
        /// </summary>
        public FCLUnmanagedCompilerTool(IList<byte> data)
            : base(data)
        { return; }

        protected unsafe override SmartPtr InitializeHandle(int sum)
        { return SmartPtr.AllocMemoryMappedView(sum, System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWriteExecute); }
    }

}
