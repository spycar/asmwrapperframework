﻿using System;
using System.Runtime.InteropServices;

namespace prebetaasm
{
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate uint DWordCall();

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate ushort WordCall();

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate byte ByteCall();

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void VoidCall();

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public unsafe delegate byte* BytePtrCall();

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public unsafe delegate uint* DWordPtrCall();

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public unsafe delegate ushort WordPtrCall();



    public interface IFinalizer
    {
        bool IsActive { get; }
        void Destroy(bool ActivateBeforeDestroy);
    }
}
