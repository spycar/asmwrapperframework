﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace prebetaasm
{
    /// <summary>
    /// Предоставляет делегат с атрибутом SecurityCritical для использования использования в компиляторах
    /// </summary>
    /// <param name="bufferposition">Позиция в буфере, начиная с которой нужно получить значение</param>
    [SecurityCritical]
    public delegate T ListingHelper<out T>(int bufferposition);


    /// <summary>
    /// Предоставляет средства JIT-компиляции неуправляемого кода
    /// </summary>
    /// <typeparam name="Tfunc">Результат компиляции. TFunc должен быть делегатом.</typeparam>
    [SecurityCritical]
    public abstract class NativeCompilerTool<Tfunc> : IDisposable
        where Tfunc : class
    {

#pragma warning disable 0618



        /// <summary>
        /// Создаёт NativeCompilerTool, используя в качестве временного буфера стек
        /// </summary>
        public unsafe NativeCompilerTool(Stack<byte> s)
        {
            if (s == null)
                throw new ArgumentNullException();

            if (!ContainsValidGenericParameter)
                throw new ArgumentException();

            GetBufferCount = s.Count<byte>;
            AddByte = s.Push;
            RemoveByte = s.Pop;
            GetResult = CreateFunction;
            IsJITLaunched = MemoryAllocationCheck;
        }


        /// <summary>
        /// Создаёт NativeCompilerTool без использования временного буфера (используя в качестве него заранее выделенную неуправляемую память), 
        /// при условии что предполагаемый размер кода в байтах известен и задан заранее.
        /// </summary>
        public unsafe NativeCompilerTool(int i)
        {
            if (i < 1)
                throw new ArgumentOutOfRangeException();

            if(!ContainsValidGenericParameter)
                throw new ArgumentException();

            Ptr = InitializeHandle(MaxBufferSize = i);
            i = Int32.MinValue;                                                            //i = 1000 0000 0000 0000 0000 0000 0000 0000

            GetBufferCount = delegate
            { return i & Int32.MaxValue; };                                                //i and 0111 1111 1111 1111 1111 1111 1111 1111

            RemoveByte = delegate
            { return unchecked(Ptr.BytePointer[(i = (int)checked((uint)i - 1))]); };       //TODO: проверить, возможны баги (должно выбрасываться при i <= 0)

            IsJITLaunched = delegate
            { return i >= 0 && Ptr.BytePointer != null; };

            AddByte = x => unchecked(Ptr.BytePointer[(i = (i & Int32.MaxValue) + 1) - 1]) = x;
            GetResult = y => y > -1 && IsJITLaunched() ? Marshal.GetDelegateForFunctionPointer(new IntPtr(Ptr.BytePointer + (i = y)), typeof(Tfunc)) as Tfunc : null;
        }




        /// <summary>
        /// Создаёт NativeCompilerTool, используя в качестве временного буфера реализацию интерфейса IList
        /// </summary>
        public unsafe NativeCompilerTool(IList<byte> s)
        {
            if (s == null)
                throw new ArgumentNullException();

            if (!ContainsValidGenericParameter || (s as System.Collections.IList).IsFixedSize)
                throw new ArgumentException();

            if (s.IsReadOnly)
                throw new MemberAccessException();


            GetBufferCount = s.Count<byte>;

            RemoveByte = delegate
            {
                byte b = s.Last();
                s.RemoveAt(GetBufferCount() - 1);
                return b;
            };

            GetResult = CreateFunction;
            IsJITLaunched = MemoryAllocationCheck;
            AddByte = s.Add;
        }

        /// <summary>
        /// Создаёт экземпляр, используя массив байтов в качестве буфера.
        /// </summary>
        /// <param name="bytes">Массив фиксированной длины</param>
        /// <param name="startIndex">Индекс, начиная с которого должна производиться запись в массив</param>
        public NativeCompilerTool(byte[] bytes, int startIndex = 0)
        {
            Enumerable.ElementAt(bytes, startIndex);    //неявная проверка и на null, и на выход startIndex за границы массива

            if (!ContainsValidGenericParameter)
                throw new ArgumentException();

            AddByte = x => bytes[startIndex++] = x;

            RemoveByte = delegate
            { return bytes[--startIndex]; };

            GetBufferCount = delegate
            { return startIndex; };

            MaxBufferSize = bytes.Length;                //так правильней
            IsJITLaunched = MemoryAllocationCheck;
            GetResult = CreateFunction;
        }

        /// <summary>
        /// Возвращает результат проверки валидности параметра Tfunc
        /// </summary>
        public static bool ContainsValidGenericParameter
        {
            get
            {
                Type t = typeof(Tfunc);
                return t.BaseType.Equals(typeof(MulticastDelegate)) && !t.ContainsGenericParameters;
            }
        }

        /// <summary>
        /// При переопределении, размещает блок системной памяти с разрешением на исполнение
        /// </summary>
        /// <param name="sum">Количество байт, выделяемых с разрешением на исполнение</param>
        /// <returns>Указатель на аллоцированную память с разрешением на исполнение</returns>
        protected abstract SmartPtr InitializeHandle(int sum);


        private SmartPtr Ptr;

        /// <summary>
        /// Увеличивает значение GetBufferCount на 1 и записывает туда указанный байт
        /// </summary>
        public readonly Action<byte> AddByte;

        /// <summary>
        /// Возвращает последний байт и уменьшает значение GetBufferCount на 1
        /// </summary>
        public readonly Func<byte> RemoveByte;

        /// <summary>
        /// Возвращает true, если осуществлялась попытка начала динамической компиляции кода. Данная функция может быть полезна при условной 
        /// компиляции, либо при проверке самого факта начала компиляции.
        /// </summary>
        public readonly Func<bool> IsJITLaunched;


        /// <summary>
        /// Максимальный допустимый размер буфера в байтах. По умолчанию равен System.Int32.MaxValue.
        /// </summary>
        public readonly int MaxBufferSize = Int32.MaxValue;

        /// <summary>
        /// Текущий размер буфера в байтах.
        /// </summary>
        public readonly Func<int> GetBufferCount;

        /// <summary>
        /// Возвращает скомпилированный из буфера с байтами делегат типа Tfunc. Можно указать смещение в буфере, начиная с которого начнётся компиляция.
        /// </summary>
        public readonly ListingHelper<Tfunc> GetResult;


        /// <summary>
        /// Возвращает сведения о том, можно ли использовать экземпляр класса повторно даже после вызова Dispose().
        /// </summary>
        public virtual bool IsReusable { get { return GetResult == CreateFunction; } }


        private unsafe Tfunc CreateFunction(int position)
        {
            int i1 = GetBufferCount();

            if ((position < 0) || (Ptr.BytePointer == null && i1 == 0))
                return null;

            if (i1 > 0)
            {
                Dispose();
                SmartPtr buffer = InitializeHandle(i1);

                for (--i1; i1 >= position; i1--)
                    buffer.BytePointer[i1] = RemoveByte();

                Ptr = buffer;
            }

            return Marshal.GetDelegateForFunctionPointer((IntPtr)Ptr.GetOffsetValue(position), typeof(Tfunc)) as Tfunc;
        }

        /// <summary>
        /// Проверяет, была ли выделена для данного экземпляра класса неуправляемая память.
        /// </summary>
        /// <returns>Возвращает true, если существует указатель на блок неуправляемой памяти; в противном случае false.</returns>
        private unsafe bool MemoryAllocationCheck()
        { return Ptr.BytePointer != null; }


        public void Dispose()
        {
            Ptr.Dispose();
            Ptr = default(SmartPtr);
        }

#pragma warning restore 0618

    }



}
