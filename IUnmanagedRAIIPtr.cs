﻿using System;
using System.IO.MemoryMappedFiles;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;

namespace prebetaasm
{
    public interface IUnmanagedMemoryRAIIPtr : IFinalizer, IDisposable
    { byte this[long l] { get; set; } }


    [System.Security.SecurityCritical]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SmartPtr : IUnmanagedMemoryRAIIPtr
    {
        public unsafe SmartPtr(UIntPtr v)
        { BytePointer = *(byte**)&v; }


        public unsafe SmartPtr(IntPtr i)
        { BytePointer = *(byte**)&i; }


        public unsafe SmartPtr(byte* v, Action<SmartPtr> act)
        {
            if (v == null)
                throw new ArgumentNullException();

            BytePointer = v;
            Finalization += act;
        }



        public unsafe SmartPtr(IntPtr i, Action<SmartPtr> act)
        {
            if (i == IntPtr.Zero)
                throw new ArgumentNullException();

            BytePointer = *(byte**)&i;
            Finalization += act;
        }



        public unsafe SmartPtr(UIntPtr i, Action<SmartPtr> act)
        {
            if (i == UIntPtr.Zero)
                throw new ArgumentNullException();

            BytePointer = *(byte**)&i;
            Finalization += act;
        }


        public unsafe SmartPtr(void* v)
        { BytePointer = (byte*)v; }


        public static unsafe explicit operator void*(SmartPtr operand1)
        { return operand1.BytePointer; }


        public static unsafe explicit operator IntPtr(SmartPtr operand1)
        { return *(IntPtr*)&operand1; }


        public static unsafe explicit operator UIntPtr(SmartPtr operand1)
        { return *(UIntPtr*)&operand1; }


        public static unsafe explicit operator byte*(SmartPtr operand1)
        { return operand1.BytePointer; }

        /// <summary>
        /// Низкоуровнево получает новый умный указатель по смещению текущего
        /// </summary>
        /// <param name="int32Offset">Смещение текущего экземпляра SmartPtr</param>
        /// <returns>Новый указатель</returns>
        public unsafe SmartPtr GetOffsetValue(int int32Offset)
        {
            void* v = BytePointer + int32Offset;
            return *(SmartPtr*)&v;
        }


        public static unsafe SmartPtr operator +(SmartPtr operand1, uint operand2)
        { return new SmartPtr(operand1.BytePointer + operand2); }

        public static unsafe SmartPtr operator -(SmartPtr operand1, uint operand2)
        { return new SmartPtr(operand1.BytePointer - operand2); }


        public static unsafe explicit operator sbyte*(SmartPtr operand1)
        { return (sbyte*)operand1.BytePointer; }


        bool IFinalizer.IsActive { get { return dict.ContainsKey(this); } }
        public readonly unsafe byte* BytePointer;
        private static ConcurrentDictionary<SmartPtr, Delegate> dict = new ConcurrentDictionary<SmartPtr, Delegate>();
        private static IFinalizer fin = new Finalizer(RemoveAllData);

        public event Action<SmartPtr> Finalization
        {
            add
            {
                if (value != null && !dict.TryAdd(this, value))
                {
                    Delegate data;
                    dict.TryGetValue(this, out data);
                    dict[this] = Delegate.Combine(a: data, b: value);
                }
            }

            remove
            {
                dict[this] = Delegate.Remove(dict[this], value);

                if (dict[this] == null)
                {
                    Delegate a;
                    dict.TryRemove(this, out a);
                }
            }
        }


        private static void RemoveAllData()
        {
            if (!dict.IsEmpty)
            {
                System.Collections.Generic.KeyValuePair<SmartPtr, Delegate>[] arr = dict.ToArray();

                for (int i1 = arr.Length - 1; i1 >= 0; i1--)
                    arr[i1].Key.InternalDestroy(true);
            }
        }

        /// <summary>
        /// Аллоцирует блок неуправляемой памяти
        /// </summary>
        /// <param name="length">Размер выделяемой памяти</param>
        /// <param name="access">Тип желаемого доступа к выделяемой памяти</param>
        /// <returns>Новый экземпляр SmartPtr с неизменяемым методом финализации</returns>
        public unsafe static SmartPtr AllocMemoryMappedView(int length, MemoryMappedFileAccess access)
        {
            MemoryMappedFile f = MemoryMappedFile.CreateNew(null, length, access);
            MemoryMappedViewAccessor a = f.CreateViewAccessor(0, length, access);
            Microsoft.Win32.SafeHandles.SafeMemoryMappedViewHandle h = a.SafeMemoryMappedViewHandle;
            byte* b = null;
            h.AcquirePointer(ref b);
            SmartPtr s = new SmartPtr(b);
            dict.TryAdd(s, new Action(h.ReleasePointer) + new Action(h.Dispose) + new Action(a.Dispose) + new Action(f.Dispose));
            return s;
        }

        void IFinalizer.Destroy(bool runbeforedestroy)
        { InternalDestroy(runbeforedestroy); }


        public unsafe byte this[long l]
        {
            get { return BytePointer[l]; }
            set { BytePointer[l] = value; }
        }


        /// <summary>
        /// Освобождает память, выделенную методом AllocMemoryMappedView
        /// </summary>
        /// <param name="ptr">Указатель на ранее выделенную память. Должен быть получен посредством AllocMemoryMappedView.</param>
        public static void FreeMemoryMappedView(SmartPtr ptr)
        {
            Delegate d;

            if (!(dict.TryGetValue(ptr, out d) && TryInvokeWithoutArgs(d)))
                throw new InvalidOperationException();

            dict.TryRemove(ptr, out d);
        }



        private static bool TryInvokeWithoutArgs(Delegate d)
        {
            bool b = d is Action;

            if (b)
                new Action(d as Action).Invoke();

            return b;
        }


        private void InternalDestroy(bool runbeforedestroy)
        {
            Delegate d;

            if (dict.TryRemove(this, out d) && runbeforedestroy && !TryInvokeWithoutArgs(d))
                new Action<SmartPtr>(d as Action<SmartPtr>).Invoke(this);
        }

        /// <summary>
        /// Финализирует данный экземпляр SmartPtr и удаляет метод его финализации из хеш-таблицы, если это возможно
        /// </summary>
        public void Dispose()
        { InternalDestroy(true); }
    }
}
