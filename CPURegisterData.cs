﻿
namespace prebetaasm
{



    //Register<T, TT> (ныне CPURegisterData<T, TT>) является generic-эквивалентом Register.cs из https://github.com/CosmosOS/XSharp
    //Само получилось
    //Во избежание коллизий класс был переименован и оставлена соответствующая заметка в файле исходного кода

    [ISAElement]
    public abstract class CPURegisterData<TT>
    {
        protected CPURegisterData(byte opcode)
        {
            if (!ABIExists)
                throw new System.ArgumentException();

            Opcode = opcode;
        }


        public static readonly bool ABIExists = typeof(TT).GetCustomAttributes(typeof(ISAProviderAttribute), false).Length > 0;
        public readonly byte Opcode;
    }

    /// <summary>
    /// Абстракция процессорного регистра
    /// </summary>
    /// <typeparam name="T">Размер машинного слова процессора</typeparam>
    /// <typeparam name="TT">Архитектура процессора</typeparam>

    [ISAElement]
    public abstract class CPURegisterData<T, TT>:CPURegisterData<TT>
        where T : struct
    {
        protected CPURegisterData(byte opcode)
            :base(opcode)
        { return; }
    }
}
