﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;

namespace prebetaasm
{
    /// <summary>
    /// Предоставляет статические методы для низкоуровневого маршалинга и ассемблирования, а также реализации IISA для конкретных платформ
    /// </summary>
    public static class ASM
    {

#pragma warning disable 0618

        private static readonly unsafe byte PointerSize = (byte)sizeof(SmartPtr);
        private const string IntErr = "32-битное приложение не поддерживает 64-битные адреса", FlErr = "Такие флаги не поддерживаются";
        private static readonly ProcessorArchitecture CurrentCPUArchitecture = Assembly.GetEntryAssembly().GetName().ProcessorArchitecture;


        /// <summary>
        /// Маршалирует управляемую платформой структуру неизвестного размера в байтовый массив. Размер структуры в байтах описывается числом размером с байт.
        /// </summary>
        /// <param name="operand">Упакованная структура</param>
        /// <returns>Байтовый массив</returns>
        public static byte[] NativeStructToBytesArray(object operand)
        { return NativeStructToBytesArray(operand, 0, checked((byte)Marshal.SizeOf(structure: operand))); }



        /// <summary>
        /// Маршалирует управляемую платформой структуру в массив байтов
        /// </summary>
        /// <param name="operand">Упакованная структура</param>
        /// <param name="startIndex">Индекс элемента создаваемого массива, на котором начнётся запись</param>
        /// <param name="size">Размер массива</param>
        /// <returns>Байтовый массив</returns>
        public static unsafe byte[] NativeStructToBytesArray(object operand, byte startIndex, byte size)
        {
            byte[] bytes = new byte[size];

            fixed (byte* ptr = bytes)
                Marshal.StructureToPtr(operand, new IntPtr(ptr + startIndex), false);

            return bytes;
        }





        /// <summary>
        /// Возвращает реализацию архитектуры набора команд IA32 процессора x86
        /// </summary>
        public static unsafe IISA<uint, IA> IA32<T>(NativeCompilerTool<T> asmblock)
            where T : class
        { return new IA_ISA<uint>(asmblock.AddByte); }


        /// <summary>
        /// Возвращает реализацию архитектуры набора команд IA16 процессора x86
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="asmblock"></param>
        /// <returns></returns>
        public static unsafe IISA<ushort, IA> IA16<T>(NativeCompilerTool<T> asmblock)
            where T : class
        { return new IA_ISA<ushort>(asmblock.AddByte); }

        /// <summary>
        /// Возвращает реализацию архитектуры набора команд IA32 для создания листинга
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static IISA<uint, IA> IA32(IList<byte> collection)
        { return new IA_ISA<uint>(collection.Add); }

        /// <summary>
        /// Возвращает реализацию архитектуры набора команд IA16 для создания листинга
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static IISA<ushort, IA> IA16(IList<byte> collection)
        { return new IA_ISA<ushort>(collection.Add); }

        /// <summary>
        /// Возвращает реализацию Long Mode для архитектуры набора команд x86-64
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="asmblock"></param>
        /// <returns></returns>
        public static IISA<ulong, IA> IA32E<T>(NativeCompilerTool<T> asmblock)
            where T : class
        { return new IA_ISA<ulong>(asmblock.AddByte); }

        /// <summary>
        /// Возвращает реализацию Long Mode для архитектуры набора команд x86-64, использующую список для создания листинга
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static IISA<ulong, IA> IA32E(IList<byte> collection)
        { return new IA_ISA<ulong>(collection.Add); }

        /// <summary>
        /// Возвращает реализацию Long Mode для архитектуры набора команд x86-64 для потоковой записи.
        /// </summary>
        /// <param name="stream">Поток ввода-вывода</param>
        /// <returns></returns>
        public static IISA<ulong, IA> IA32E(Stream stream)
        { return new IA_ISA<ulong>(stream.WriteByte); }



        public static IISA<uint,IA> IA32(Stream stream)
        { return new IA_ISA<uint>(stream.WriteByte); }


        public static IISA<ushort,IA> IA16(Stream stream)
        { return new IA_ISA<ushort>(stream.WriteByte); }

        public static IISA<byte,IA> IA8(Stream stream)
        { return new IA_ISA<byte>(stream.WriteByte); }

        public static IISA<byte,IA> IA8(IList<byte> collection)
        { return new IA_ISA<byte>(collection.Add); }


        public static IISA<byte, IA> IA8<T>(NativeCompilerTool<T> asmblock)
            where T : class
        { return new IA_ISA<byte>(asmblock.AddByte); }


        private struct IA_ISA<TSize> : IISA<TSize, IA>
            where TSize : struct
        {
            public IA_ISA(Action<byte> a)
                : this()
            {
                if (PointerSize < ArgSize)
                    throw new NotSupportedException(IntErr);

                RawDataAdd = a;

                switch (ArgSize)
                {
                    case 1:
                        InstructionDataAdd = WriteOpcodeFor8BitMode;
                        return;

                    case 2:
                        InstructionDataAdd = ChangeProcessorMode;
                        return;

                    case 4:
                        InstructionDataAdd = RawDataAdd;
                        return;

                    case 8:
                        InstructionDataAdd = WriteOpcodeWithREX;
                        return;
                }
            }





            /// <summary>
            /// Макрокоманда для запись восьми байтов по указателю. В будущем может быть заинлайнена ради производительности.
            /// </summary>
            /// <param name="pointer"></param>
            private unsafe void WriteQWord(byte* pointer)
            {
                RawDataAdd(pointer[0]);
                RawDataAdd(pointer[1]);
                RawDataAdd(pointer[2]);
                RawDataAdd(pointer[3]);
                RawDataAdd(pointer[4]);
                RawDataAdd(pointer[5]);
                RawDataAdd(pointer[6]);
                RawDataAdd(pointer[7]);
            }

            /// <summary>
            /// Макрокоманда для записи 4-х байтов по указателю. В будущем может быть заинлайнена для производительности.
            /// </summary>
            /// <param name="pointer"></param>
            private unsafe void WriteDWord(byte* pointer)
            {
                RawDataAdd(pointer[0]);
                RawDataAdd(pointer[1]);
                RawDataAdd(pointer[2]);
                RawDataAdd(pointer[3]);
            }


            private void WriteOpcodeWithREX(byte opcode)
            {
                RawDataAdd(0x48);                               //Префикс REX для 64-битного режима
                RawDataAdd(opcode);
            }

            private void WriteOpcodeFor8BitMode(byte opcode)    //В большинстве команд с reg8 у байта опкода просто отнимается младший бит
            { RawDataAdd(opcode &= 0xfe); }                     //Было решено всё-таки создать отдельный режим для IA8 и сыграть на этом



            private void ChangeProcessorMode(byte b)
            {
                RawDataAdd(0x66);                               //Префикс 16-битного режима
                RawDataAdd(b);
            }


            /// <summary>
            /// Мега-функция для обработки любых команд с использованием MRM с косвенной адресацией.
            /// </summary>
            /// <param name="opc1">Обязательный код операции.</param>
            /// <param name="opc2">Опциональный операции при работе с константой. В случае, если аргумент слишком велик, 
            /// предлагается использовать макрокоманду, заранее загружая его в один из свободных регистров; соотв. данный опкод должен 
            /// содержать операцию с регистром вместо константы; игнорируется, если аргумент не указан.</param>
            /// <param name="reg">Поле reg</param>
            /// <param name="rm">Поле rm</param>
            /// <param name="offset">Смещение.</param>
            /// <param name="arg">Константа. Опциональна. В целях производительности можно записать константу отдельно, уже после вызова 
            /// данного метода.</param>
            private unsafe void WriteOpcodeMRM(byte opc1, byte opc2, CPURegisterData<IA> reg, CPURegisterData<IA> rm, int offset, object arg)
            {
                unchecked
                {
                    bool* conds = stackalloc bool[3];
                    conds[0] = rm is CPURegisterData<uint, IA>;
                    conds[1] = PointerSize == 8;

                    if (*(ushort*)conds == 0)
                        throw new NotSupportedException(IntErr);

                    if (conds[2] = (arg is ulong && (ulong)arg > 0x7fffffff))
                    {
                        RawDataAdd((byte)(0x50 | (reg = rm.Opcode == IA.AX.Opcode ? IA.BX : IA.AX).Opcode));       //push rm == rax ? rbx : rax
                        WriteOpcodeWithREX((byte)(0xb8 | reg.Opcode));                                             //mov rax, arg
                        Array.ForEach(NativeStructToBytesArray(arg, 0, 8), RawDataAdd);
                        opc1 = opc2;
                        arg = null;
                    }

                    if (*(ushort*)conds == 257)
                        RawDataAdd(0x67);

                    InstructionDataAdd(opc1);
                    RawDataAdd(IA.MRM(conds[0] = (offset < 128 && offset > -129), reg, rm));

#if false                                                     //Явные операции с указателем стека отложены до лучших времён

                if (rm.Opcode == IA.AH.Opcode)                               //Если rm == sp, должен стоять "пустой" байт SIB
                    RawDataAdd(0x24);

#endif

                    if (conds[0])
                        RawDataAdd((byte)checked((sbyte)offset));

                    else
                        WriteDWord((byte*)&offset);

                    if (arg != null)
                        WriteNativeArgFormat(arg);

                    else if (conds[2])
                        RawDataAdd((byte)(0x58 | reg.Opcode));         //Восстановление стека и регистра rax/rbx если они использовались
                }
            }

            /// <summary>
            /// Метод для трансляции инструкций с двумя операндами-регистрами либо с регистром и константой.
            /// </summary>
            /// <param name="opcode">Стандартный код операции</param>
            /// <param name="opcode2">Код операции, пригодный при использовании с 64-битной константой. Опционален. Должен быть 
            /// аналогичной инструкцией, но с двумя регистрами.</param>
            /// <param name="reg">Поле reg</param>
            /// <param name="rm">Поле rm</param>
            /// <param name="arg">Аргумент. Опционален.</param>
            private unsafe void WriteOpcodeWithRegistersAndArg(byte opcode, byte opcode2, CPURegisterData<IA> reg, CPURegisterData<IA> rm, object arg)
            {

                if (arg is ulong && (ulong)arg > 0x7fffffff)
                {
                    byte* bytes = stackalloc byte[8];
                    Marshal.StructureToPtr(arg, new IntPtr(bytes), false);
                    WriteOperationWithStackPointer(0xc7, IA.AX, -8);
                    WriteDWord(bytes);
                    WriteOperationWithStackPointer(0xc7, IA.AX, -4);
                    WriteDWord(bytes + 4);
                    RawDataAdd(0x48);                                                    //mov [rsp - 8], arg
                    WriteOperationWithStackPointer(opcode2, rm, -8);                     //opcode2 rm, [rsp - 8]
                }

                else
                {
                    InstructionDataAdd(opcode);
                    RawDataAdd(IA.MRM(reg, rm));

                    if (arg != null)
                        WriteNativeArgFormat(arg);
                }
            }


            /// <summary>
            /// Отвечает за операции с прямой адресацией. Использование прямых указателей в 64-битном режиме разворачивается в макрокоманду 
            /// и снижает производительность.
            /// </summary>
            /// <param name="opcode">Основной код операции</param>
            /// <param name="opcode2">Дополнительный код операции. При работе с большими константами следует указывать в качестве этого 
            /// кода аналогичную операцию, но между указателем и регистром.</param>
            /// <param name="reg">Поле reg.</param>
            /// <param name="pointer">Указатель. Обязан соответствовать стандартному размеру указателя. Значение null не допускается.</param>
            /// <param name="arg">Константа. Внимание: при наличии в операции аргумента-константы 
            /// её указание здесь обязательно! В остальных случаях ставить null.</param>
            private unsafe void WriteOpcodeWithPtr(byte opcode, byte opcode2, CPURegisterData<IA> reg, object pointer, object arg)
            {
                byte* pointerbytes = stackalloc byte[PointerSize];
                Marshal.StructureToPtr(pointer, new IntPtr(pointerbytes), false);

                if (PointerSize == 8)
                {
                    byte* b = stackalloc byte[4];                                    // если rax уже задействован, используется rbx
                    RawDataAdd(unchecked((byte)(*(int*)b = reg.Opcode == IA.AX.Opcode ? 0x5b03bb53 : 0x5800b850)));      // push rax
                    WriteOpcodeWithREX(b[1]);                                             //mov rax, pointer 
                    WriteQWord(pointerbytes);


                    if (arg is ulong && (ulong)arg > 0x7fffffff)        //если операция с указателем и аргументом, то регистры де-факто не используются
                    {
                        RawDataAdd(0x52);                                                  //push rdx
                        WriteOpcodeWithREX(0xba);                                          //mov rdx, arg
                        Marshal.StructureToPtr(arg, new IntPtr(pointerbytes), false);      //Возможна утечка памяти, следует изучить этот момент лучше
                        WriteQWord(pointerbytes);
                        InstructionDataAdd(opcode2);
                        RawDataAdd(b[2] |= 0x10);                                          //*opcode* rdx, [rax]
                        RawDataAdd(0x5a);                                                  //pop rdx
                    }

                    else
                    {
                        InstructionDataAdd(opcode);
                        RawDataAdd(unchecked((byte)(b[2] | reg.Opcode << 3)));             // *opcode* reg, [rax]

                        if (arg != null)
                            WriteNativeArgFormat(arg);                       
                    }

                    RawDataAdd(b[3]);                                                            // pop rax
                }

                else
                {
                    InstructionDataAdd(opcode);
                    RawDataAdd(IA.MRM(reg));
                    WriteDWord(pointerbytes);

                    if (arg != null)
                        Array.ForEach(NativeStructToBytesArray(arg), RawDataAdd);
                }
            }


            private unsafe void WriteOperationWithStackPointer(byte opcode, CPURegisterData<IA> reg, sbyte sp_offset)
            {
                RawDataAdd(opcode);
                RawDataAdd(IA.MRM(true, reg, IA.AH));
                RawDataAdd(0x24);
                RawDataAdd(unchecked((byte)sp_offset));
            }



            private unsafe void WritePushOrPopWithPtr(byte opcode, CPURegisterData<IA> reg, sbyte offset, void* pointer)
            {

                if (PointerSize == 8)
                {
                    RawDataAdd(0x48);
                    WriteOperationWithStackPointer(0x89, IA.AX, -16);                //mov [rsp - 16], rax
                    WriteOpcodeWithREX(0xb8);
                    WriteQWord((byte*)&pointer);                                                 //mov rax, pointer
                    RawDataAdd(opcode);
                    opcode = reg.Opcode;
                    RawDataAdd(opcode <<= 3);                                                            //opcode reg, [rax]
                    RawDataAdd(0x48);
                    WriteOperationWithStackPointer(0x8b, IA.AX, unchecked(offset -= 16));       //mov rax, [rsp + offset -= 16]
                }

                else
                {
                    RawDataAdd(opcode);
                    RawDataAdd(IA.MRM(reg));
                    WriteDWord((byte*)&pointer);
                }

            }



            private void WriteNativeArgFormat(object arg)
            {
                byte[] argBytes = NativeStructToBytesArray(arg);

                if (argBytes.Length < 4)
                    Array.ForEach(argBytes, RawDataAdd);

                else
                {
                    RawDataAdd(argBytes[0]);
                    RawDataAdd(argBytes[1]);
                    RawDataAdd(argBytes[2]);
                    RawDataAdd(argBytes[3]);
                }

            }


            private Action<byte> RawDataAdd, InstructionDataAdd;
            private static byte ArgSize = unchecked((byte)Marshal.SizeOf(t: typeof(TSize)));







            public void MOV(CPURegisterData<TSize, IA> reg, CPURegisterData<TSize, IA> rm)
            {
                InstructionDataAdd(0x8b);
                RawDataAdd(IA.MRM(reg, rm));
            }






            public unsafe void MOV(CPURegisterData<TSize, IA> reg, TSize data)
            {
                bool cond = ArgSize != 1;
                (cond ? InstructionDataAdd : RawDataAdd).Invoke(unchecked((byte)(0xb0 | (*(byte*)&cond << 3) | reg.Opcode)));
                Array.ForEach(NativeStructToBytesArray(data, 0, ArgSize), RawDataAdd);
            }




            public unsafe void MOV(SmartPtr v, TSize i)
            { WriteOpcodeWithPtr(0xc7, 0x89, IA.AX, v, i); }




            public unsafe void MOV(CPURegisterData<TSize, IA> reg, SmartPtr v)
            { WriteOpcodeWithPtr(0x8b, 0, reg, v, null); }

            public unsafe void MOV(SmartPtr v, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeWithPtr(0x89, 0, rm, v, null); }



            public unsafe void MOV(CPURegisterData<TSize, IA> reg, CPURegisterData<uint, IA> rmaddress, int offset)
            { WriteOpcodeMRM(0x8b, 0, reg, rmaddress, offset, null); }

            public unsafe void MOV(CPURegisterData<TSize, IA> reg, CPURegisterData<ulong, IA> rmaddress, int offset)
            { WriteOpcodeMRM(0x8b, 0, reg, rmaddress, offset, null); }


            public unsafe void MOV(CPURegisterData<uint, IA> regaddress, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(0x89, 0, rm, regaddress, offset, null); }

            public unsafe void MOV(CPURegisterData<ulong, IA> regaddress, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(0x89, 0, rm, regaddress, offset, null); }




            public unsafe void MOV(CPURegisterData<uint, IA> regaddress, int offset, TSize data)
            { WriteOpcodeMRM(0xc7, 0x89, IA.AX, regaddress, offset, data); }


            public unsafe void MOV(CPURegisterData<ulong, IA> regaddress, int offset, TSize data)
            { WriteOpcodeMRM(0xc7, 0x89, IA.AX, regaddress, offset, data); }





            public void PUSH(CPURegisterData<TSize, IA> reg)
            {
                RawDataAdd(0x48);
                WriteOperationWithStackPointer(0x8d, IA.AH, checked((sbyte)-ArgSize));       //lea rsp, [rsp - 4]
                InstructionDataAdd(0x89);
                RawDataAdd(IA.MRM(true, reg, IA.AH));
                RawDataAdd(0x24);
                RawDataAdd(0);                                                               //mov [rsp + 0], reg              
            }



            public unsafe void PUSH(TSize data)
            {
                byte[] bytes = NativeStructToBytesArray(data, 0, ArgSize);


                if (PointerSize == 8)
                    RawDataAdd(0x48);

                WriteOperationWithStackPointer(0x8d, IA.AH, checked((sbyte)-ArgSize));       //lea rsp, [rsp - ArgSize]

                if (ArgSize == 8)
                {
                    WriteOperationWithStackPointer(0xc7, IA.AX, 0);
                    RawDataAdd(bytes[0]);
                    RawDataAdd(bytes[1]);
                    RawDataAdd(bytes[2]);
                    RawDataAdd(bytes[3]);
                    WriteOperationWithStackPointer(0xc7, IA.AX, 4);
                    RawDataAdd(bytes[4]);
                    RawDataAdd(bytes[5]);
                    RawDataAdd(bytes[6]);
                    RawDataAdd(bytes[7]);                                                     //mov dword ptr [rax + 4], bytes
                }

                else
                {
                    InstructionDataAdd(0xc7);
                    RawDataAdd(IA.AH.Opcode);
                    RawDataAdd(0x24);                                                         //mov [rsp], reg
                    Array.ForEach(bytes, RawDataAdd);
                }
            }



            public unsafe void PUSH(UIntPtr* pointer)
            { WritePushOrPopWithPtr(0xff, IA.SI, 8, pointer); }


            public unsafe void PUSH(CPURegisterData<uint, IA> reg, int offset)
            { new IA_ISA<TSize>() { RawDataAdd = RawDataAdd, InstructionDataAdd = RawDataAdd }.WriteOpcodeMRM(0xff, 0, IA.SI, reg, offset, null); }

            public unsafe void PUSH(CPURegisterData<ulong, IA> reg, int offset)
            { new IA_ISA<TSize>() { RawDataAdd = RawDataAdd, InstructionDataAdd = RawDataAdd }.WriteOpcodeMRM(0xff, 0, IA.SI, reg, offset, null); }


            public unsafe void POP(UIntPtr* pointer)
            { WritePushOrPopWithPtr(0x8f, IA.AX, -8, pointer); }

            public unsafe void POP(CPURegisterData<uint, IA> reg, int offset)
            { new IA_ISA<TSize>() { RawDataAdd = RawDataAdd, InstructionDataAdd = RawDataAdd }.WriteOpcodeMRM(0x8f, 0, IA.AX, reg, offset, null); }


            public unsafe void POP(CPURegisterData<ulong, IA> reg, int offset)
            { new IA_ISA<TSize>() { RawDataAdd = RawDataAdd, InstructionDataAdd = RawDataAdd }.WriteOpcodeMRM(0x8f, 0, IA.AX, reg, offset, null); }


            public unsafe void POP(CPURegisterData<TSize, IA> reg)
            {
                InstructionDataAdd(0x8b);
                RawDataAdd(IA.MRM(true, reg, IA.AH));
                RawDataAdd(0x24);
                RawDataAdd(0);                                                                  //mov reg, [rsp + 0]

                if (PointerSize == 8)
                    RawDataAdd(0x48);

                WriteOperationWithStackPointer(0x8d, IA.AH, unchecked((sbyte)ArgSize));         //lea rsp, [rsp + ArgSize]
            }


            public unsafe void JMP(int offset)
            {
                unchecked
                {
                    if (offset < 128 && offset > -129)
                    {
                        RawDataAdd(0xeb);
                        RawDataAdd((byte)checked((sbyte)offset));
                    }

                    else
                    {
                        RawDataAdd(0xe9);
                        WriteDWord((byte*)&offset);
                    }
                }
            }




            public unsafe void JMP(SmartPtr addr)
            {
                WriteOperationWithStackPointer(0xc7, IA.AX, -8);
                byte* ptr = (byte*)&addr;
                WriteDWord(ptr);

                if (PointerSize == 8)
                {
                    WriteOperationWithStackPointer(0xc7, IA.AX, -4);
                    WriteDWord(ptr + 4);
                }

                WriteOperationWithStackPointer(0xff, IA.AH, -8);
            }

            public unsafe void JMP(CPURegisterData<uint, IA> reg, int offset)
            { WriteOpcodeMRM(0xff, 0, IA.AH, reg, offset, null); }

            public unsafe void JMP(CPURegisterData<ulong, IA> reg, int offset)
            { WriteOpcodeMRM(0xff, 0, IA.AH, reg, offset, null); }

            public void JMP(CPURegisterData<TSize, IA> reg)
            {
                if (ArgSize == PointerSize || ArgSize == 2)
                {
                    InstructionDataAdd(0xff);
                    RawDataAdd(IA.MRM(IA.AH, reg));
                }

                else
                {
                    if (PointerSize == 8)
                        RawDataAdd(0x48);

                    WriteOperationWithStackPointer(0xc7, IA.AX, -8);
                    RawDataAdd(0);
                    RawDataAdd(0);
                    RawDataAdd(0);
                    RawDataAdd(0);                                                         //mov dword ptr [rsp - 4], 0
                    WriteOpcodeMRM(0x89, 0, reg, IA.AH, -8, null);                         //mov [rsp - 8], reg
                    WriteOperationWithStackPointer(0xff, IA.AH, -8);                       //jmp [rsp - 8]
                }
            }


            public unsafe void CALL(int offset)
            {
                RawDataAdd(0xe8);
                WriteDWord((byte*)&offset);
            }


            public unsafe void CALL(Delegate d)
            {
                IntPtr i = Marshal.GetFunctionPointerForDelegate(d);
                WriteOperationWithStackPointer(0xc7, IA.AX, -8);                       //mov [rsp - 8], i
                WriteDWord((byte*)&i);

                if (PointerSize == 8)
                {
                    WriteOperationWithStackPointer(0xc7, IA.AX, -4);
                    WriteDWord(4 + (byte*)&i);
                }

                WriteOperationWithStackPointer(0xff, IA.DX, -8);                       //call [rsp - 8]
            }


            public void CALL(CPURegisterData<TSize, IA> reg)
            {
                if (ArgSize == PointerSize || ArgSize == 2)
                {
                    InstructionDataAdd(0xff);
                    RawDataAdd(IA.MRM(IA.DX, reg));
                }

                else
                {
                    if (PointerSize == 8)
                        RawDataAdd(0x48);

                    WriteOperationWithStackPointer(0xc7, IA.AX, -8);
                    RawDataAdd(0);
                    RawDataAdd(0);
                    RawDataAdd(0);
                    RawDataAdd(0);
                    WriteOpcodeMRM(0x89, 0, reg, IA.AH, -8, null);
                    WriteOperationWithStackPointer(0xff, IA.DX, -8);
                }
            }




            public void ADD(CPURegisterData<TSize, IA> reg, CPURegisterData<TSize, IA> rm)
            {
                InstructionDataAdd(3);
                RawDataAdd(IA.MRM(reg, rm));
            }



            public unsafe void ADD(CPURegisterData<TSize, IA> reg, TSize data)
            { WriteOpcodeWithRegistersAndArg(0x81, 1, IA.AX, reg, data); }


            public unsafe void ADD(CPURegisterData<TSize, IA> reg, SmartPtr v)
            { WriteOpcodeWithPtr(3, 0, reg, v, null); }


            public unsafe void ADD(SmartPtr v, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeWithPtr(1, 0, rm, v, null); }






            public unsafe void ADD(SmartPtr v, TSize i)
            { WriteOpcodeWithPtr(0x81, 1, IA.AX, v, i); }                             // 81 01 00 00 (0x00000181)


            public unsafe void SUB(SmartPtr v, TSize i)
            { WriteOpcodeWithPtr(0x81, 0x29, IA.BP, v, i); }



            public unsafe void SUB(SmartPtr v, CPURegisterData<TSize, IA> RM)
            { WriteOpcodeWithPtr(0x29, 0, RM, v, null); }



            public unsafe void SUB(CPURegisterData<TSize, IA> reg, SmartPtr v)
            { WriteOpcodeWithPtr(0x2b, 0, reg, v, null); }


            public void SUB(CPURegisterData<TSize, IA> reg, CPURegisterData<TSize, IA> rm)
            {
                InstructionDataAdd(0x2b);
                RawDataAdd(IA.MRM(reg, rm));
            }

            public unsafe void SUB(CPURegisterData<TSize, IA> reg, TSize data)
            { WriteOpcodeWithRegistersAndArg(0x81, 0x29, IA.BP, reg, data); }

            public unsafe void ADD(CPURegisterData<TSize, IA> reg, CPURegisterData<uint, IA> rmaddress, int offset)
            { WriteOpcodeMRM(3, 0, reg, rmaddress, offset, null); }


            public unsafe void ADD(CPURegisterData<TSize, IA> reg, CPURegisterData<ulong, IA> rmaddress, int offset)
            { WriteOpcodeMRM(3, 0, reg, rmaddress, offset, null); }



            public unsafe void ADD(CPURegisterData<uint, IA> regaddress, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(1, 0, rm, regaddress, offset, null); }

            public unsafe void ADD(CPURegisterData<ulong, IA> regaddress, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(1, 0, rm, regaddress, offset, null); }




            public unsafe void ADD(CPURegisterData<uint, IA> regaddress, int offset, TSize data)
            { WriteOpcodeMRM(0x81, 1, IA.AX, regaddress, offset, data); }

            public unsafe void ADD(CPURegisterData<ulong, IA> regaddress, int offset, TSize data)
            { WriteOpcodeMRM(0x81, 1, IA.AX, regaddress, offset, data); }


            public unsafe void SUB(CPURegisterData<TSize, IA> reg, CPURegisterData<uint, IA> rmaddress, int offset)
            { WriteOpcodeMRM(0x28, 0, reg, rmaddress, offset, null); }

            public unsafe void SUB(CPURegisterData<TSize, IA> reg, CPURegisterData<ulong, IA> rmaddress, int offset)
            { WriteOpcodeMRM(0x28, 0, reg, rmaddress, offset, null); }


            public unsafe void SUB(CPURegisterData<ulong, IA> regoffset, int ptr, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(0x29, 0, rm, regoffset, ptr, null); }

            public unsafe void SUB(CPURegisterData<uint, IA> regoffset, int ptr, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(0x29, 0, rm, regoffset, ptr, null); }





            public unsafe void SUB(CPURegisterData<ulong, IA> regaddress, int offset, TSize data)
            { WriteOpcodeMRM(0x81, 0x29, IA.BP, regaddress, offset, data); }

            public unsafe void SUB(CPURegisterData<uint, IA> regaddress, int offset, TSize data)
            { WriteOpcodeMRM(0x81, 0x29, IA.BP, regaddress, offset, data); }


            public void LEA(CPURegisterData<uint, IA> reg, CPURegisterData<uint, IA> rm, int offset)
            { new IA_ISA<TSize>() { RawDataAdd = RawDataAdd, InstructionDataAdd = RawDataAdd }.WriteOpcodeMRM(0x8d, 0, reg, rm, offset, null); }


            public void LEA(CPURegisterData<ulong, IA> reg, CPURegisterData<ulong, IA> rm, int offset)
            { new IA_ISA<TSize>() { RawDataAdd = RawDataAdd, InstructionDataAdd = WriteOpcodeWithREX }.WriteOpcodeMRM(0x8d, 0, reg, rm, offset, null); }


            public void LEA(CPURegisterData<ulong, IA> reg, UIntPtr h)
            { new IA_ISA<ulong>(RawDataAdd).WriteOpcodeWithPtr(0x8d, 0, reg, h, null); }


            public void LEA(CPURegisterData<uint, IA> reg, UIntPtr h)
            { new IA_ISA<TSize>() { RawDataAdd = RawDataAdd, InstructionDataAdd = RawDataAdd }.WriteOpcodeWithPtr(0x8d, 0, reg, h, null); }



            public unsafe void OR(CPURegisterData<TSize, IA> reg, CPURegisterData<uint, IA> rm, int offset)
            { WriteOpcodeMRM(0xb, 0, reg, rm, offset, null); }


            public unsafe void OR(CPURegisterData<TSize, IA> reg, CPURegisterData<ulong, IA> rm, int offset)
            { WriteOpcodeMRM(0xb, 0, reg, rm, offset, null); }


            public unsafe void OR(CPURegisterData<TSize, IA> reg, SmartPtr pointer)
            { WriteOpcodeWithPtr(0xb, 0, reg, pointer, null); }

            public unsafe void OR( CPURegisterData<uint, IA> reg, int offset,CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(9, 0, rm, reg, offset, null); }

            public unsafe void OR( CPURegisterData<ulong, IA> reg, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(9, 0, rm, reg, offset, null); }







            public unsafe void OR(SmartPtr pointer, CPURegisterData<TSize, IA> reg)
            { WriteOpcodeWithPtr(9, 0, reg, pointer, null); }



            public void OR(SmartPtr pointer, TSize data)
            { WriteOpcodeWithPtr(0x81, 9, IA.CX, pointer, data); }



            public void OR(CPURegisterData<TSize,IA> reg, CPURegisterData<TSize,IA> rm)
            { 
                InstructionDataAdd(0x0b);
                RawDataAdd(IA.MRM(reg, rm));
            }


            public void OR(CPURegisterData<TSize,IA> reg,TSize data)
            { WriteOpcodeWithRegistersAndArg(0x81, 9, IA.CX, reg, data); }


            public void OR(CPURegisterData<uint,IA> reg, int offset, TSize arg)
            { WriteOpcodeMRM(0x81, 9, IA.CX, reg, offset, arg); }

            public void OR(CPURegisterData<ulong,IA> reg,int offset,TSize arg)
            { WriteOpcodeMRM(0x81, 9, IA.CX, reg, offset, arg); }









            public unsafe void XOR(CPURegisterData<TSize, IA> reg, CPURegisterData<uint, IA> rm, int offset)
            { WriteOpcodeMRM(0x33, 0, reg, rm, offset, null); }


            public unsafe void XOR(CPURegisterData<TSize, IA> reg, CPURegisterData<ulong, IA> rm, int offset)
            { WriteOpcodeMRM(0x33, 0, reg, rm, offset, null); }


            public unsafe void XOR(CPURegisterData<TSize, IA> reg, SmartPtr pointer)
            { WriteOpcodeWithPtr(0x33, 0, reg, pointer, null); }

            public unsafe void XOR(CPURegisterData<uint, IA> reg, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(0x31, 0, rm, reg, offset, null); }

            public unsafe void XOR(CPURegisterData<ulong, IA> reg, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(0x31, 0, rm, reg, offset, null); }







            public unsafe void XOR(SmartPtr pointer, CPURegisterData<TSize, IA> reg)
            { WriteOpcodeWithPtr(0x31, 0, reg, pointer, null); }



            public void XOR(SmartPtr pointer, TSize data)
            { WriteOpcodeWithPtr(0x81, 0x31, IA.SI, pointer, data); }



            public void XOR(CPURegisterData<TSize, IA> reg, CPURegisterData<TSize, IA> rm)
            {
                InstructionDataAdd(0x33);
                RawDataAdd(IA.MRM(reg, rm));
            }


            public void XOR(CPURegisterData<TSize, IA> reg, TSize data)
            { WriteOpcodeWithRegistersAndArg(0x81, 0x31, IA.SI, reg, data); }


            public void XOR(CPURegisterData<uint, IA> reg, int offset, TSize arg)
            { WriteOpcodeMRM(0x81, 0x31, IA.SI, reg, offset, arg); }

            public void XOR(CPURegisterData<ulong, IA> reg, int offset, TSize arg)
            { WriteOpcodeMRM(0x81, 0x31, IA.SI, reg, offset, arg); }
















            public unsafe void AND(CPURegisterData<TSize, IA> reg, CPURegisterData<uint, IA> rm, int offset)
            { WriteOpcodeMRM(0x23, 0, reg, rm, offset, null); }


            public unsafe void AND(CPURegisterData<TSize, IA> reg, CPURegisterData<ulong, IA> rm, int offset)
            { WriteOpcodeMRM(0x23, 0, reg, rm, offset, null); }


            public unsafe void AND(CPURegisterData<TSize, IA> reg, SmartPtr pointer)
            { WriteOpcodeWithPtr(0x23, 0, reg, pointer, null); }

            public unsafe void AND(CPURegisterData<uint, IA> reg, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(0x21, 0, rm, reg, offset, null); }

            public unsafe void AND(CPURegisterData<ulong, IA> reg, int offset, CPURegisterData<TSize, IA> rm)
            { WriteOpcodeMRM(0x21, 0, rm, reg, offset, null); }







            public unsafe void AND(SmartPtr pointer, CPURegisterData<TSize, IA> reg)
            { WriteOpcodeWithPtr(0x21, 0, reg, pointer, null); }



            public void AND(SmartPtr pointer, TSize data)
            { WriteOpcodeWithPtr(0x81, 0x21, IA.AH, pointer, data); }



            public void AND(CPURegisterData<TSize, IA> reg, CPURegisterData<TSize, IA> rm)
            {
                InstructionDataAdd(0x23);
                RawDataAdd(IA.MRM(reg, rm));
            }


            public void AND(CPURegisterData<TSize, IA> reg, TSize data)
            { WriteOpcodeWithRegistersAndArg(0x81, 0x21, IA.AH, reg, data); }


            public void AND(CPURegisterData<uint, IA> reg, int offset, TSize arg)
            { WriteOpcodeMRM(0x81, 0x21, IA.AH, reg, offset, arg); }

            public void AND(CPURegisterData<ulong, IA> reg, int offset, TSize arg)
            { WriteOpcodeMRM(0x81, 0x21, IA.AH, reg, offset, arg); }






            public void INC(CPURegisterData<TSize, IA> reg)
            {
                InstructionDataAdd(0xff);
                RawDataAdd(IA.MRM(IA.AX, reg));
            }


            public void INC(CPURegisterData<TSize,IA> reg, int offset)
            { WriteOpcodeMRM(0xff, 0, IA.AX, reg, offset, null); }

            public void INC(SmartPtr pointer)
            { WriteOpcodeWithPtr(0xff, 0, IA.AX, pointer, null); }





            public void DEC(CPURegisterData<TSize, IA> reg)
            {
                InstructionDataAdd(0xff);
                RawDataAdd(IA.MRM(IA.CX, reg));
            }


            public void DEC(CPURegisterData<TSize, IA> reg, int offset)
            { WriteOpcodeMRM(0xff, 0, IA.CX, reg, offset, null); }

            public void DEC(SmartPtr pointer)
            { WriteOpcodeWithPtr(0xff, 0, IA.CX, pointer, null); }





            public unsafe void J(CPUFlagData flags, int offset)
            {
                byte flagsbyte = (byte)flags;

                if (flagsbyte == (flagsbyte & 0xef))                        //Проверка маски 00010000
                    throw new NotSupportedException(FlErr);

                flagsbyte &= 0xf;                                              //Удаление маски

                if (offset < 128 && offset > -129)
                {
                    RawDataAdd(flagsbyte |= 0x70);
                    RawDataAdd(unchecked((byte)checked((sbyte)offset)));
                }

                else
                {
                    RawDataAdd(0xf);
                    RawDataAdd(flagsbyte |= 0x80);
                    WriteDWord((byte*)&offset);
                }
            }



            public void ENTER()
            {                
                RawDataAdd(0xc8);
                RawDataAdd(0);
                RawDataAdd(0);
                RawDataAdd(0);
            }


            public void RET()
            { RawDataAdd(0xc3); }


            public void NOP()
            { RawDataAdd(0x90); }

            public unsafe void ENTER(ushort length)
            {
                RawDataAdd(0xc8);
                RawDataAdd(*(byte*)&length);
                RawDataAdd(*(1 + (byte*)&length));
                RawDataAdd(0);
            }

            public void LEAVE()
            { RawDataAdd(0xc9); }
        }

#pragma warning restore 0618

    }
}
