﻿using System;

namespace prebetaasm
{
    /// <summary>
    /// Отмечает элемент, соответствующий спецификации набора команд процессора
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
    public class ISAElementAttribute : Attribute
    {
        public ISAElementAttribute()
        { return; }
    }
}
