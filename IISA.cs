﻿using System;

namespace prebetaasm
{
    /// <summary>
    /// Предоставляет оболочки для ассемблирования инструкция процессора в соответствии с указанной архитектурой,
    /// с возможностью их дальнейшего исполнения (например, с помощью NativeCompilerTool)
    /// </summary>
    /// <typeparam name="Tword">Размер машинного слова</typeparam>
    /// <typeparam name="TABI">Данные архитектуры процессора</typeparam>
   
    [ISAElement]
    public interface IISA<Tword, TABI>
        where Tword : struct
    {
        void MOV(CPURegisterData<Tword, TABI> reg, CPURegisterData<Tword, TABI> rm);


        void MOV(CPURegisterData<Tword, TABI> reg, CPURegisterData<uint, TABI> rmaddress, int offset);
        void MOV(CPURegisterData<uint, TABI> regaddress, int offset, CPURegisterData<Tword, TABI> rm);
        void MOV(CPURegisterData<uint, TABI> regaddress, int offset, Tword data);


        void MOV(CPURegisterData<ulong, TABI> regaddress, int offset, CPURegisterData<Tword, TABI> rm);
        void MOV(CPURegisterData<ulong, TABI> regaddress, int offset, Tword data);
        void MOV(CPURegisterData<Tword, TABI> reg, CPURegisterData<ulong, TABI> rmaddress, int offset);


        void JMP(SmartPtr addr);
        void JMP(int offset);
        void JMP(CPURegisterData<Tword, TABI> register);
        void JMP(CPURegisterData<uint, TABI> regaddress, int offset);
        void JMP(CPURegisterData<ulong, TABI> regaddress, int offset);
        void CALL(Delegate d);
        void CALL(int offset);
        void CALL(CPURegisterData<Tword, TABI> register);
        void J(CPUFlagData cond, int offset);



        void ADD(CPURegisterData<Tword, TABI> reg, CPURegisterData<uint, TABI> rmaddress, int offset);
        void ADD(CPURegisterData<uint, TABI> regaddress, int offset, CPURegisterData<Tword, TABI> rm);
        void ADD(CPURegisterData<uint, TABI> regaddress, int offset, Tword data);



        void ADD(CPURegisterData<ulong, TABI> regaddress, int offset, CPURegisterData<Tword, TABI> rm);
        void ADD(CPURegisterData<ulong, TABI> regaddress, int offset, Tword data);
        void ADD(CPURegisterData<Tword, TABI> reg, CPURegisterData<ulong, TABI> rmaddress, int offset);

        void SUB(CPURegisterData<Tword, TABI> reg, CPURegisterData<uint, TABI> rmaddress, int offset);
        void SUB(CPURegisterData<uint, TABI> regaddress, int offset, CPURegisterData<Tword, TABI> rm);
        void SUB(CPURegisterData<uint, TABI> regaddress, int offset, Tword data);


        void SUB(CPURegisterData<ulong, TABI> regaddress, int offset, CPURegisterData<Tword, TABI> rm);
        void SUB(CPURegisterData<ulong, TABI> regaddress, int offset, Tword data);
        void SUB(CPURegisterData<Tword, TABI> reg, CPURegisterData<ulong, TABI> rmaddress, int offset);


        void MOV(CPURegisterData<Tword, TABI> reg, Tword data);
        void ADD(CPURegisterData<Tword, TABI> reg, CPURegisterData<Tword, TABI> rm);
        void ADD(CPURegisterData<Tword, TABI> reg, Tword data);
        void SUB(CPURegisterData<Tword, TABI> reg, CPURegisterData<Tword, TABI> rm);
        void SUB(CPURegisterData<Tword, TABI> reg, Tword data);


        void PUSH(CPURegisterData<Tword, TABI> reg);
        void PUSH(CPURegisterData<uint, TABI> reg, int offset);
        void PUSH(CPURegisterData<ulong,TABI> reg, int offset);
        void PUSH(Tword data);
        unsafe void PUSH(UIntPtr* pointer);
        

        void POP(CPURegisterData<Tword, TABI> reg);
        void POP(CPURegisterData<uint, TABI> reg, int offset);
        void POP(CPURegisterData<ulong, TABI> reg, int offset);
        unsafe void POP(UIntPtr* pointer);



        void MOV(SmartPtr v, CPURegisterData<Tword, TABI> rm);
        void MOV(CPURegisterData<Tword, TABI> reg, SmartPtr v);
        void ADD(SmartPtr v, CPURegisterData<Tword, TABI> rm);
        void ADD(CPURegisterData<Tword, TABI> reg, SmartPtr v);
        void SUB(CPURegisterData<Tword, TABI> reg, SmartPtr v);
        void SUB(SmartPtr v, CPURegisterData<Tword, TABI> rm);
        void MOV(SmartPtr v, Tword i);
        void SUB(SmartPtr v, Tword i);
        void ADD(SmartPtr v, Tword i);
        void NOP();
        void RET();
        void ENTER();
        void ENTER(ushort length);
        void LEAVE();


        void LEA(CPURegisterData<ulong, TABI> reg, CPURegisterData<ulong, TABI> rm, int offset);
        void LEA(CPURegisterData<uint, TABI> reg, CPURegisterData<uint, TABI> rm, int offset);
        void LEA(CPURegisterData<ulong, TABI> reg, UIntPtr pointer);
        void LEA(CPURegisterData<uint, TABI> reg, UIntPtr pointer);


        void OR(CPURegisterData<Tword, TABI> reg, CPURegisterData<Tword, TABI> rm);
        void OR(CPURegisterData<ulong, TABI> reg, int offset, CPURegisterData<Tword, TABI> rm);
        void OR(CPURegisterData<uint, TABI> reg, int offset, CPURegisterData<Tword, TABI> rm);
        void OR(CPURegisterData<Tword, TABI> reg, CPURegisterData<ulong, TABI> rm, int offset);
        void OR(CPURegisterData<Tword, TABI> reg, CPURegisterData<uint, TABI> rm, int offset);
        void OR(CPURegisterData<Tword, TABI> reg, SmartPtr pointer);
        void OR(CPURegisterData<Tword, TABI> reg, Tword data);
        void OR(SmartPtr pointer, CPURegisterData<Tword, TABI> rm);
        void OR(SmartPtr pointer, Tword data);


        void XOR(CPURegisterData<Tword, TABI> reg, CPURegisterData<Tword, TABI> rm);
        void XOR(CPURegisterData<ulong, TABI> reg, int offset, CPURegisterData<Tword, TABI> rm);
        void XOR(CPURegisterData<uint, TABI> reg, int offset, CPURegisterData<Tword, TABI> rm);
        void XOR(CPURegisterData<Tword, TABI> reg, CPURegisterData<ulong, TABI> rm, int offset);
        void XOR(CPURegisterData<Tword, TABI> reg, CPURegisterData<uint, TABI> rm, int offset);
        void XOR(CPURegisterData<Tword, TABI> reg, SmartPtr pointer);
        void XOR(CPURegisterData<Tword, TABI> reg, Tword data);
        void XOR(SmartPtr pointer, CPURegisterData<Tword, TABI> rm);
        void XOR(SmartPtr pointer, Tword data);




        void AND(CPURegisterData<Tword, TABI> reg, CPURegisterData<Tword, TABI> rm);
        void AND(CPURegisterData<ulong, TABI> reg, int offset, CPURegisterData<Tword, TABI> rm);
        void AND(CPURegisterData<uint, TABI> reg, int offset, CPURegisterData<Tword, TABI> rm);
        void AND(CPURegisterData<Tword, TABI> reg, CPURegisterData<ulong, TABI> rm, int offset);
        void AND(CPURegisterData<Tword, TABI> reg, CPURegisterData<uint, TABI> rm, int offset);
        void AND(CPURegisterData<Tword, TABI> reg, SmartPtr pointer);
        void AND(CPURegisterData<Tword, TABI> reg, Tword data);
        void AND(SmartPtr pointer, CPURegisterData<Tword, TABI> rm);
        void AND(SmartPtr pointer, Tword data);



        void INC(CPURegisterData<Tword, TABI> reg);
        void INC(CPURegisterData<Tword, TABI> reg, int offset);
        void INC(SmartPtr pointer);
        void DEC(CPURegisterData<Tword, TABI> reg);
        void DEC(CPURegisterData<Tword, TABI> reg, int offset);
        void DEC(SmartPtr pointer);
    }

    /// <summary>
    /// Флаги процессора. Константы из данного перечисления применимы и в x86, и в ARM-архитектурах. 
    /// Младшие 4 бита обозначают processor-specific состояние флагов условий, старшие 4 бита - маски, применяемые при идентификации 
    /// процессора, для которого они предназначены (*reserved*, *reserved*, ARM, Intel Architecture соответственно).
    /// В перечислении хранятся только флаги, применимые в большинстве архитектур (с маской 0xf0 = 11110000); 
    /// processor-specific-flags хранятся в соотв. статических классах.
    /// </summary>
    [Flags]
    [ISAElement]
    public enum CPUFlagData : byte
    { Carry = 2 | 0xf0, Overflow = 0xf0 }


    /// <summary>
    /// Предоставляет дополнительный функционал состояния флагов
    /// </summary>
    public static class CPUFlagDataHelper
    {
        /// <summary>
        /// Предоставляет флаг отрицания для битовых масок состояния флагов. Метод полностью идентичен CPUFlagDataHelper.Unset().
        /// </summary>
        /// <param name="flag">Условие</param>
        /// <returns>Условие с отрицанием</returns>
        public static CPUFlagData FALSE(this CPUFlagData flag)
        { return flag | (CPUFlagData)1; }

        /// <summary>
        /// Предоставляет флаг отрицания для битовых масок состояния флагов. Метод полностью идентичен CPUFlagDataHelper.FALSE().
        /// </summary>
        /// <param name="flag">Условие</param>
        /// <returns>Условие с отрицанием</returns>
        public static CPUFlagData Unset(this CPUFlagData flag)
        { return flag | (CPUFlagData)1; }
    }
}


