﻿using System.Collections.Generic;

namespace prebetaasm
{
    namespace Win32MemoryManagementTools
    {
        /// <summary>
        /// Реализация NativeCompilerTool с использованием WinAPI
        /// </summary>
        /// <typeparam name="TFunc">Результат компиляции. TFunc должен быть делегатом.</typeparam>
        [System.Security.SecurityCritical]
        public class WinRTUnmanagedCompilerTool<TFunc> : NativeCompilerTool<TFunc>
            where TFunc : class
        {
            /// <summary>
            /// Создаёт NativeCompilerTool, используя в качестве временного буфера стек
            /// </summary>
            public WinRTUnmanagedCompilerTool(Stack<byte> s)
                : base(s)
            { return; }

            /// <summary>
            /// Создаёт NativeCompilerTool, используя в качестве временного буфера массив фиксированной длины
            /// </summary>
            /// <param name="bytes">Массив</param>
            /// <param name="i">Индекс, начиная с которого производится работа с массивом.</param>
            public WinRTUnmanagedCompilerTool(byte[] bytes, int i = 0)
                : base(bytes, i)
            { return; }

            /// <summary>
            /// Создаёт NativeCompilerTool без использования временного буфера, при условии что предполагаемый размер кода в байтах известен и задан заранее
            /// </summary>
            public WinRTUnmanagedCompilerTool(int i)
                : base(i)
            { return; }

            /// <summary>
            /// Создаёт NativeCompilerTool, используя в качестве временного буфера реализацию интерфейса IList
            /// </summary>
            public WinRTUnmanagedCompilerTool(IList<byte> data)
                : base(data)
            { return; }


            protected override unsafe SmartPtr InitializeHandle(int i)
            { return SafeWin32HeapHandle.GetImplicitHeapMemory(Win32HeapOptions.EnableExecute, i); }
        }
    }
}
