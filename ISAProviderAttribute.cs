﻿using System;

namespace prebetaasm
{
    /// <summary>
    /// Маркирует класс, предоставляющий элементы архитектуры набора команд
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class ISAProviderAttribute : ISAElementAttribute
    {
        public ISAProviderAttribute()
        { return; }
    }
}
