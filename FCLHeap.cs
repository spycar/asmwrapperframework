﻿using System;
using System.Runtime.InteropServices;

namespace prebetaasm
{
    /// <summary>
    /// Предоставляет готовые способы работы со стандартными кучами .Net Framework.
    /// </summary>
    public class FCLHeap : IHeap
    {
        private FCLHeap(Func<int, IntPtr> alloc, Func<IntPtr, int, IntPtr> realloc, Action<IntPtr> free)
        {
            Alloc = alloc;
            ReAlloc = realloc;
            Free = x => free((IntPtr)x);
        }


        private Func<int, IntPtr> Alloc;
        private Func<IntPtr, int, IntPtr> ReAlloc;
        private Action<SmartPtr> Free;

        /// <summary>
        /// Предоставляет доступ к куче COM-взаимодействия текущего процесса
        /// </summary>
        public static readonly FCLHeap CoTaskMem = new FCLHeap(Marshal.AllocCoTaskMem, Marshal.ReAllocCoTaskMem, Marshal.FreeCoTaskMem);

        /// <summary>
        /// Предоставляет доступ к глобальной куче текущего процесса
        /// </summary>
        public static readonly FCLHeap HGlobal = new FCLHeap(Marshal.AllocHGlobal, (x, y) => Marshal.ReAllocHGlobal(x, new IntPtr(y)), Marshal.FreeHGlobal);

        /// <summary>
        /// Выделяет блок памяти посредством AllocHGlobal или AllocCoTaskMem с FreeHGlobal или FreeCoTaskMem в качестве методов финализации.
        /// </summary>
        /// <param name="size"></param>
        /// <returns>
        /// Указатель с методом финализации. Безопасность использования не гарантируется в случае вызова на данном указателе
        /// ReAllocHGlobal, ReAllocCoTaskMem,
        /// FreeHGlobal или FreeCoTaskMem (в этих случаях рекомендуется вызвать Destroy(false) на этом указателе посредством явной реализации IFinalizer)
        /// </returns>
        public SmartPtr AllocHeap(int size)
        { return new SmartPtr(Alloc(size), Free); }


        /// <summary>
        /// Освобождает блок памяти текущей кучи
        /// </summary>
        /// <param name="ptr">
        /// Умный указатель на блок освобождаемой памяти.
        /// Может быть также освобождён посредством вызова Dispose или Destroy(true) на себе.
        /// </param>
        public void FreeHeap(SmartPtr ptr)
        {
            ptr.Finalization -= Free;
            Free(ptr);
        }


        /// <summary>
        /// Изменяет размер блока памяти, аллоцированного ранее в глобальной куче процесса или в COM
        /// </summary>
        /// <param name="oldmemory">Указатель на старую память</param>
        /// <param name="newSize">Желаемый размер обновлённого блока памяти</param>
        /// <returns> 
        /// Указатель с методом финализации. Безопасность использования не гарантируется в случае вызова на данном указателе
        /// ReAllocHGlobal, ReAllocCoTaskMem,
        /// FreeHGlobal или FreeCoTaskMem (в этих случаях рекомендуется вызвать Destroy(false) на этом указателе посредством явной реализации IFinalizer)
        /// </returns>
        public SmartPtr ReAllocHeap(SmartPtr oldmemory, int newSize)
        {
            oldmemory.Finalization -= Free;
            return new SmartPtr(ReAlloc((IntPtr)oldmemory, newSize), Free);
        }
    }
}
