﻿using System;

namespace prebetaasm
{
    /// <summary>
    /// Класс, инкапсулирующий делегат без параметров и запускающий его при своей финализации
    /// </summary>
    public class Finalizer : IFinalizer
    {
        /// <summary>
        /// Создаёт новый экземпляр класса Finalizer
        /// </summary>
        /// <param name="act">Действие, которое должно быть выполнено</param>
        public Finalizer(Action act)
        { h = act; }


        private Action h;
        public bool IsActive { get { return h != null; } }

        public void Destroy(bool activatebeforedestroy)
        {
            if (activatebeforedestroy && IsActive)
                h.Invoke();

            h = null;
            GC.SuppressFinalize(this);
        }



        ~Finalizer()
        {
            if (IsActive)
                h.Invoke();
        }
    }
}
