﻿
namespace prebetaasm
{
        [System.Security.SecurityCritical]
        public interface IHeap
        {
            unsafe SmartPtr AllocHeap(int size);
            unsafe void FreeHeap(SmartPtr block);
            unsafe SmartPtr ReAllocHeap(SmartPtr block, int newSize);
        }
}
